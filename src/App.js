import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Home from './pages/Home'
import LoginResto from './pages/LoginResto'
import SignUp from './pages/SignUp'
import CreateNewResto from './pages/CreateNewResto'
import NotFound from './pages/NotFound'
import LoginCustomer from "./pages/LoginCustomer";
import CustomerApp from "./pages/CustomerApp";
import EmployeeList from "./pages/EmployeeList"
import EmployeeDetails from "./pages/EmployeeDetails"
import EmployeeAdd from "./pages/EmployeeAdd"
import MenuView from "./pages/MenuView"
import MenuDetail from './pages/MenuDetail'
import MenuAdd from './pages/MenuAdd'

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/resto/home" component={Home} />
          <Route exact path="/resto/home/:id" component={Home} />
          <Route exact path="/resto/login" component={LoginResto} />
          <Route exact path="/resto/SignUp" component={SignUp} />
          <Route exact path="/resto/register" component={CreateNewResto} />
          <Route exact path="/customer/add-customer" component={LoginCustomer} />
          <Route exact path="/order" component={CustomerApp} />
          <Route exact path="/resto/employees" component={EmployeeList} />
          <Route exact path="/resto/employees/add" component={EmployeeAdd} />
          <Route exact path="/resto/employees/:id" component={EmployeeDetails} />
          <Route exact path="/resto/menu" component={MenuView} />
          <Route exact path="/resto/menu/add" component={MenuAdd} />
          <Route exact path="/resto/menu/:id" component={MenuDetail} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    );
  }
}

export default App;
