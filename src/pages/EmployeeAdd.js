import React, { Component } from 'react';
import { Container } from 'reactstrap'

import "../App.css";

class EmployeeAdd extends Component {
    state = {
        name: '',
        email: '',
        password: '',
        phone: '',
        position: ''
    }
    set = name => event => this.setState({ [name]: event.target.value })
    handleSubmit = event => {
        event.preventDefault()
        // const { name, email, password, phone, position } = this.state
        // const { history } = this.props
        console.log(this.state)

        // // .then(response => console.log(response))
        // .then(json => {
        //     history.push('/' + id)
        // })
        // .catch(err => { alert(err) }
        // )
    }


    render() {
        return (
            <Container className='App'>
                <h1>Add New Employee</h1>
                <form onSubmit={this.handleSubmit}>
                    <label>Name:
                        <br />
                        <input type="text" name="name" onChange={this.set('name')} />
                    </label>
                    <br />

                    <label>Email
                        <br />
                        <input type="email" name="email" onChange={this.set('email')} />
                    </label>
                    <br />
                    <label>Password
                        <br />
                        <input type="password" name="password" onChange={this.set('password')} />
                    </label>
                    <br />
                    <label>Phone
                        <br />
                        <input type="tel" name="phone" onChange={this.set('phone')} />
                    </label>
                    <br />

                    <label>Position
                        <br />
                        <input type="text" name="position" onChange={this.set('position')} />
                    </label>
                    <br />


                    <input type="submit" value="submit" />
                </form>
            </Container>
        )
    }
}

export default EmployeeAdd