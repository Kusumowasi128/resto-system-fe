import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Container } from 'reactstrap'

import MenuInput from '../component/MenuInput'


class EmployeeDetails extends Component {

    state = {
        id: this.props.match.params.id,
        name: '',
        joinDate: '',
        email: '',
        phone: '',
        position: ''
    }

    render() {
        return (
            <Container>
                <h1>Employee Name</h1>
                <p>Join date: {this.state.joinDate}</p>
                <MenuInput inputName="Email" rows="1" defaultInput={this.state.email} />
                <MenuInput inputName="Phone" rows="1" defaultInput={this.state.phone} />
                <MenuInput inputName="Position" rows="1" defaultInput={this.state.position} />
                <br />
                <div className="d-flex flex-column">
                    <Link to="../employees" className="btn btn-primary" >Return to employee list</Link>
                    <Link className="my-1 btn btn-danger">Remove employee</Link>
                </div>
            </Container>
        )
    }
}

export default EmployeeDetails