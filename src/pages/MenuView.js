import React, { Component } from 'react';
import { Container } from 'reactstrap'
import MenuComponent from '../component/MenuComponent'

import { Link } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css';

const DATA = [
    {
        id: '1',
        name: 'bakso',
        price: 10000,
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod unde quisquam facilis rerum ea nobis numquam pariatur fugiat facere qui?'
    },
    {
        id: '2',
        name: 'jagung',
        price: 12000,
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod unde quisquam facilis rerum ea nobis numquam pariatur fugiat facere qui?'
    },
    {
        id: '3',
        name: 'kimchi',
        price: 30000,
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod unde quisquam facilis rerum ea nobis numquam pariatur fugiat facere qui?'
    }
]

class MenuView extends Component {

    render() {
        return (
            <Container className='App'>
                <h1>View Menu</h1>
                <Link to="/resto/menu/add" className='btn btn-primary w-100 my-2' >Add new menu</Link>
                <Link to="/resto/home" className='btn btn-secondary w-100 my-2' >Return to main menu</Link>
                <MenuComponent menuList={DATA} />
            </Container>
        )
    }
}

export default MenuView