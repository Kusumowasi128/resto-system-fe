import React, { Component } from 'react'
import { Col } from 'react-bootstrap';
import MenuType from '../component/MenuType';
// import { API_URL } from './utils/constants';
// import axios from 'axios';

export default class TipeMenu extends Component {
  state = {
    tipeMenu:[]
  }

  componentDidMount(){
    fetch('https://resto-api-wave6.herokuapp.com/api/menu-type')
    .then(response => response.json())
    .then(result=>{this.setState({tipeMenu: result})})
  }
  render() {
    return (
        <Col md={2} mt="2">
          <h4><strong>Tipe Menu</strong></h4>
          <hr/>
          {
            this.state.tipeMenu.map(element => { return (
              <MenuType namaType={element.namaType} key={element.id}/>
            )})
          }
        </Col>
    )
  }
}
