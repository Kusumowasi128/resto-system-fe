import React, { Component } from 'react';
import { Container, Button } from "reactstrap"

import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';


class MenuAdd extends Component {
    state = {
        id: '1',
        name: 'bakso',
        price: 10000,
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod unde quisquam facilis rerum ea nobis numquam pariatur fugiat facere qui?'
    }

    render() {
        return (
            <Container>
                <h1>Menu Detail</h1>
                <form action="">
                    <label htmlFor="name">
                        <strong>Menu name:</strong>
                    </label>
                    <br />
                    <input type="text" name="name" />
                    <br />
                    <label htmlFor="price">
                        <strong>Price:</strong>
                    </label>
                    <br />
                    <input type="text" name="price" />
                    <br />
                    <label htmlFor="description">
                        <strong>Description:</strong>
                    </label>
                    <br />
                    <input type="text" name="description" />
                    <br />
                    <strong>Menu image:</strong>
                    <br />
                    <Button>Upload</Button>
                </form>

                <Link to="/resto/menu" className="btn btn-secondary" >Return to menu</Link>
            </Container>
        )
    }
}

export default MenuAdd