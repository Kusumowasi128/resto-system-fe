import React, { Component } from 'react';
import { Container, Button } from 'reactstrap'

import MenuInput from '../component/MenuInput'
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';


class MenuDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            id: '1',
            name: 'bakso',
            price: 120000,
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod unde quisquam facilis rerum ea nobis numquam pariatur fugiat facere qui?'
        };
    }




    render() {
        return (
            <Container>
                <h1>Menu Detail</h1>
                <MenuInput InputType='true' inputName="Menu name" rows="1" defaultInput={this.state.name} />
                <MenuInput inputName="Menu price" rows="1" defaultInput={this.state.price} />
                <MenuInput inputName="Menu description" rows="5" defaultInput={this.state.description} />

                <img className="w-50" src="https://picsum.photos/1920/1080" alt="" />
                <br />
                <Link to="/resto/menu" className="btn btn-secondary" >Return to menu</Link>
            </Container >
        )
    }
}

export default MenuDetail