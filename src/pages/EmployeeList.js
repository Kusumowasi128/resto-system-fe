import React, { Component } from "react"
import { Container } from "reactstrap"

import NameHolderComponent from "../component/NameHolderComponent"
import { Link } from "react-router-dom"

import "../App.css"
import 'bootstrap/dist/css/bootstrap.css';

const DATA = [
    {
        id: 1,
        name: "John",
        position: "Owner"
    },
    {
        id: 2,
        name: "Jane",
        position: "Teman"
    },
    {
        id: 3,
        name: "Jables",
        position: "Peliharaan"
    }
]

class EmployeeList extends Component {

    state = {
        restoName: ''
    }

    render() {
        return (
            <Container>
                <h1>{this.state.restoName} Employees</h1>
                <div>
                    <NameHolderComponent employees={DATA} />
                </div>
                <Link to="/resto/home" className="btn btn-secondary w-100" >Return to main menu</Link>
            </Container>
        )
    }
}

export default EmployeeList