import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import { Pesanan, TipeMenu, Menu } from "../component";

// import { API_URL } from './utils/constants';
// import axios from 'axios';

export default class CustomerApp extends Component {
  state = {
    menus: [],
    order: [],
    // categoriYangDipilih: 'makanan'
  };

  componentDidMount() {
    fetch("https://resto-api-wave6.herokuapp.com/api/menu")
      .then((response) => response.json())
      .then((result) => {
        this.setState({ menus: result });
      });
  }

  // changeCategory = (value) =>{
  //   this.setState({
  //     categoriYangDipilih: value,
  //     menus: []
  //   })
  // }

  orderanMasuk = (value) => {
    console.log("Menu :", value);
  };

  render() {
    return (
      <div className="App">
        <Row>
          <TipeMenu />
          <Col>
            <h4>
              <strong>Daftar Menu</strong>
            </h4>
            <hr />
            {this.state.menus.map((element) => {
              return (
                <Menu
                  namaMenu={element.namaMenu}
                  harga={element.harga}
                  deskripsi={element.deskripsi}
                  key={element.id}
                  orderanMasuk={this.orderanMasuk}
                />
              );
            })}
          </Col>
          <Pesanan />
        </Row>
      </div>
    );
  }
}
