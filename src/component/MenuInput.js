import React, { Component } from 'react';

class MenuInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = { value: '' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        alert('A name was submitted: ' + this.state.value);
        event.preventDefault();
    }


    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    {this.props.inputName}:
                    <br />
                    <textarea type="text" defaultValue={this.props.defaultInput} onChange={this.handleChange} rows={this.props.rows} />
                </label>
                <br />
                <input type="submit" value={"Edit " + this.props.inputName} />
            </form>
        );
    }
}

export default MenuInput