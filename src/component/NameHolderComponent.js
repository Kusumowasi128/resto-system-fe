import React, { Component } from 'react';
import { Container, Button } from 'reactstrap'
import { Link } from 'react-router-dom'


import 'bootstrap/dist/css/bootstrap.css';
import "../App.css";

export default function NameHolderComponent(props) {
    const employees = props.employees.map((employee) =>
        <Link to={"/resto/employees/" + employee.id} key={employee.id} className="btn btn-primary d-flex justify-content-between my-2" >
            <p>
                {employee.name}
            </p>
            <p>
                {employee.position}
            </p>
        </Link >
    )

    return (
        <div>
            {employees}
        </div>
    )
}