import React from 'react';
import { Col, Card, Button } from 'react-bootstrap'
import { numberWithCommas } from '../utils/koma';




const Menu = (props) => {
    return (
        <Col md={8} xr={6} className="mb-4">
            <Card className='shadow'> 
                <Card.Body>
                    <Card.Title>{props.namaMenu}</Card.Title>
                    <Card.Text>
                    {props.deskripsi} - Rp.{numberWithCommas(props.harga)}
                    </Card.Text>
                    <Button variant="primary">Pesan</Button>
                </Card.Body>
            </Card>
        </Col>
    )
}
export default Menu

    