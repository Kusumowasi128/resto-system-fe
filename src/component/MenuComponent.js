import React, { Component } from 'react';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button
} from 'reactstrap';

import { Link } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css';
import "../App.css";

export default function MenuComponent(props) {
    const menuList = props.menuList.map((menuItem) =>
        <div key={menuItem.id} className="my-3">
            <Card>
                <CardImg top width="100%" height='20%' src="https://picsum.photos/1920/1080" alt="Card image cap" />
                <CardBody>
                    <CardTitle tag="h5">{menuItem.name}</CardTitle>
                    <CardSubtitle tag="h6" className="mb-2 text-muted">Rp {menuItem.price}</CardSubtitle>
                    <CardText>{menuItem.description}</CardText>
                    <Link to={"/resto/menu/" + menuItem.id} className='btn btn-primary'>Edit Menu</Link>
                </CardBody>
            </Card>
        </div>
    )
    return (
        <div>
            {menuList}
        </div>
    )
}